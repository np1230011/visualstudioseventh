﻿#include <iostream>
#include <string>
using namespace std;

class Animal
{
protected:
    string Text = "Nope";
public:
    Animal(): Text("Different animals speak differently\n")
    {}

    virtual void Voice()
    {
        cout <<""<< Text<<"\n";
    }

};

class Dog : public Animal
{

public:
    Dog()
    {
        Text = "Woof!";
    }

    void Voice() override
    {
        cout << "Dog says:"<<Text<<"\n";
    }
};

class Cat : public Animal
{

public:
    Cat()
    {
        Text = "Meow!";
    }

    void Voice() override
    {
        cout << "Cat says:" << Text << "\n";
    }
};

class Cow : public Animal
{

public:
    Cow()
    {
        Text = "Moo!";
    }

    void Voice() override
    {
        cout << "Cow says:" << Text << "\n";
    }
};

int
 main()
{
    Animal** anim = new Animal*[3];
    Dog Sharik; Cat Matroskin; Cow Murka;
    Dog* _Sharik = &Sharik; Cat* _Matroskin = &Matroskin; Cow* _Murka = &Murka;
    anim[0] =new Dog(); //То есть Dog я не могу преобразовать в родительский класс?
    anim[1] = new Cat();
    anim[2] = new Cow;
    for (int i = 0; i < 3; i++)
    {
        anim[i]->Voice();
    }
    delete[] anim;
}

